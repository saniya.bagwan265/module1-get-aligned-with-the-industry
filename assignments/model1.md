**Module1 - Get aligned with Industry**

1. **Face Detection** : Face detection also called facial detection is an artificial intelligence (AI) based computer technology used to find and identify human faces in digital images.
- Face detection technology can be applied to various fields including *security*, *biometrics*, *law enforcement*, *entertainment* and *personal safety* to provide surveillance and tracking of people in real time. 
-A facial recognition system uses **biometrics** to map facial features from a photograph or video. It compares the information with a database of known faces to find a match.

2. **Face Recognition**: Face recognition is the science which involves the understanding of how the faces are recognized by biological systems and how this can be emulated by computer systems.
-Face recognition works same as the Face detection.

3. **Object Detection**: Object Detection is a common Computer Vision problem which deals with *identifying* and locating object of certain classes in the image.
-Interpreting the object localisation can be done in various ways, including creating a bounding box around the object or marking every pixel in the image which contains the object (called segmentation).

4. **Segmentation** : Segmentation means to divide the marketplace into parts, or segments, which are definable, accessible, actionable, and profitable and have a growth potential.
- In a company it would find it impossible to target the entire market, because of time, cost and effort restrictions. - It needs to have a 'definable' segment - a mass of people who can be identified and targeted with reasonable effort, cost and time.

5. **ID Recognition** : Id recognition is used to ensure that the person is real.
- where we can use facial recognition to ensure that the person is real.
6. **AI word Recognition** : As AI is used every where we can use AI to recognition of many more things one of that is word recognition by giving some inputs of Handwritten words to a program and to process it helps us to identified the words.
