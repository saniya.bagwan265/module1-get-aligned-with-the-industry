**Convolutional Neural Network(CNN)**


***CNNs*** is the most popular neural network model being used for image classification problem. The big idea behind CNNs is that a local understanding of an image is good enough. 
The practical benefit is that having fewer parameters greatly improves the time it takes to learn as well as reduces the amount of data required to train the model.
Instead of a fully connected network of weights from each pixel, a CNN has just enough weights to look at a small patch of the image.


**Weights**
Each neuron in a neural network computes an output value by applying a specific function to the input values coming from the receptive field in the previous layer. The function that is applied to the input values is determined by a vector of weights and a bias (typically real numbers). Learning, in a neural network, progresses by making iterative adjustments to these biases and weights.


**How Do Convolutional Neural Networks Work?**
There are four layered concepts we should understand in Convolutional Neural Networks:

1. ***Convolution***,
2. ***ReLu***,
3. ***Pooling*** and
4. ***Full Connectedness*** (Fully Connected Layer).


1. **Convolution**
The term convolution refers to the mathematical combination of two functions to produce a third function. It merges two sets of information. In the case of a CNN, the convolution is performed on the input data with the use of a filter or kernel (these terms are used interchangeably) to then produce a feature map.

When programming a CNN, the input is a tensor with shape (number of images) x (image height) x (image width) x (image depth). Then after passing through a convolutional layer, the image becomes abstracted to a feature map, with shape (number of images) x (feature map height) x (feature map width) x (feature map channels).



2. **ReLu:**
*Rectified Linear Unit* (ReLU) transform function only activates a node if the input is above a certain quantity, while the input is below zero, the output is zero, but when the input rises above a certain threshold, it has a linear relationship with the dependent variable.


3. **Pooling**
A pooling layer is another building block of a CNN. Its function is to progressively reduce the spatial size of the representation to reduce the amount of parameters and computation in the network. Pooling layer operates on each feature map independently.


4. **Full Connectedness**
At the end of a CNN, the output of the last Pooling Layer acts as input to the so called Fully Connected Layer. There can be one or more of these layers (“fully connected” means that every node in the first layer is connected to every node in the second layer).
